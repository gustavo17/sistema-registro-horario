#pragma once 

#include <string>
#include <sqlite3.h>
#include <functional>
#include "queryBase.h"


class QuerySQLite : public QueryBase{
    public:
        QuerySQLite()=default; //default no necesito implementar el constructor
        virtual ~QuerySQLite(); // default no necesito implementar el destructor

        void ejecutarQuery(     std::string sql, 
                                std::function<void()> filtros,
                                std::function<void()> resultados) override;
        void setTextPars(int col,const std::string & parametro)  override;
        void setRealPars(int col, double valor)  override;

        std::string getText(int col) const override;
        double getReal(int col) const override;

    private:
        sqlite3_stmt *stmt=nullptr;
};