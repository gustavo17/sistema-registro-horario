
#include <opencv2/opencv.hpp>
#include <string>

class Imagen {
    public:
      Imagen(const std::string imagenBase64);

      void mostrarImagen() const;

    private:
      cv::Mat img;
};