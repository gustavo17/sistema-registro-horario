#pragma once
#include <memory>
#include "queryBase.h"

class QueryFactory {
    public:
        static std::unique_ptr<QueryBase> getQuery() ;
};