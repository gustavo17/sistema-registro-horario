#include <iostream>
#include <memory>
#include "DB.h" 

std::unique_ptr<DB> & DB::getInstance(){
    if (instancia==nullptr) {
        instancia=std::unique_ptr<DB>(new DB()); // la primera vez y despues se ocupa el mismo siempre
    }
    return instancia; // todas las veces se retorna el mismo objeto
}


DB::DB() {
    int rc;

    rc = sqlite3_open_v2("./datos/registro.db",&db,SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE | SQLITE_OPEN_SHAREDCACHE ,nullptr);
    
    if( rc ) {
        throw std::runtime_error("No se pudo abrir la base de datos");
    }
    std::cout<<"conectado exitosamente a la base de datos registro.db"<<std::endl;
};


DB::~DB(){
     sqlite3_close_v2(db);
     std::cout<<"se cierra la conexion a la base de datos registro.db"<<std::endl;
};

sqlite3 *DB::getConnection(){
    return db;
};