#include <sqlite3.h>
#include <iostream>
#include "querySQLite.h"
#include "DB.h"


void QuerySQLite::ejecutarQuery(std::string sql,
                          std::function<void()> filtros,
                          std::function<void()> resultados) {

  int rc;

  //el stmt se almacena en variable de instancia para reutilizacion en filtros y resultados
  rc = sqlite3_prepare_v3(DB::getInstance()->getConnection(), sql.c_str(),
                          sql.size() + 1, SQLITE_PREPARE_PERSISTENT, &stmt,
                          nullptr);
  if (rc != SQLITE_OK)
    throw std::runtime_error("\nno se pudo preparar stmt\n");

  filtros(); // binding del sql

  // ejecutar
  rc = sqlite3_step(stmt);
  
  //ESTO VA A MOSTRAR EL ERROR
  if (rc != SQLITE_DONE && rc != SQLITE_ROW) {
    // error recordar liberar statement
    sqlite3_finalize(stmt);
    stmt=nullptr;
    throw std::runtime_error("\n" + std::string(sqlite3_errmsg(DB::getInstance()->getConnection())) +
                             "\nError en la ejecucion del sql\n");
  }

  if (rc==SQLITE_ROW) {
    do {
        resultados(); //se llama resultados() n veces
    } while (sqlite3_step(stmt) == SQLITE_ROW);
  }

  sqlite3_finalize(stmt);
  stmt=nullptr;
};

void QuerySQLite::setTextPars(int col,const std::string & parametro) {
  sqlite3_bind_text(stmt,col,parametro.c_str(),parametro.size(),SQLITE_STATIC);
}

void QuerySQLite::setRealPars(int col, double valor) {
  sqlite3_bind_double(stmt,col,valor);
}

//no usa el std::move en retornos, para activar la optimizacion por compilador
//preguntar
std::string QuerySQLite::getText(int col) const{
  //return std::string{"hola"};
  return std::string{reinterpret_cast<const char *>(sqlite3_column_text(stmt,col))};
}

double QuerySQLite::getReal(int col) const{
  return sqlite3_column_double(stmt, col);
}
QuerySQLite::~QuerySQLite(){
  std::cout<<"destruyendo al objeto QuerySQLite"<<std::endl;
};