
#include <vector>
#include <sstream>
#include "base64.h"


std::string base64_encode(const unsigned char *bytes_to_encode, unsigned int in_len) {
    std::string ret;
    int i = 0, j = 0;
    unsigned char char_array_3[3];
    unsigned char char_array_4[4];

    while (in_len--) {
        char_array_3[i++] = *(bytes_to_encode++);
        if (i == 3) {
            char_array_4[0] = (char_array_3[0] & 0xfc) >> 2;
            char_array_4[1] = ((char_array_3[0] & 0x03) << 4) + ((char_array_3[1] & 0xf0) >> 4);
            char_array_4[2] = ((char_array_3[1] & 0x0f) << 2) + ((char_array_3[2] & 0xc0) >> 6);
            char_array_4[3] = char_array_3[2] & 0x3f;

            for (i = 0; i < 4; i++)
                ret += base64_chars[char_array_4[i]];
            i = 0;
        }
    }

    if (i) {
        for (j = i; j < 3; j++)
            char_array_3[j] = '\0';

        char_array_4[0] = (char_array_3[0] & 0xfc) >> 2;
        char_array_4[1] = ((char_array_3[0] & 0x03) << 4) + ((char_array_3[1] & 0xf0) >> 4);
        char_array_4[2] = ((char_array_3[1] & 0x0f) << 2) + ((char_array_3[2] & 0xc0) >> 6);

        for (j = 0; j < i + 1; j++)
            ret += base64_chars[char_array_4[j]];

        while (i++ < 3)
            ret += '=';
    }

    return ret;
}

std::string base64_decode(const std::string& encoded_string)
{
    std::istringstream stream(encoded_string);
    std::vector<unsigned char> base64_decode_table(256, 0);

    // Construir la tabla de decodificación
    for (size_t i = 0; i < 64; ++i) {
        base64_decode_table[base64_chars[i]] = static_cast<unsigned char>(i);
    }

    // Decodificar la cadena Base64
    std::string decoded;
    unsigned char buffer[4];
    while (stream.rdbuf()->in_avail() && stream >> buffer[0] >> buffer[1] >> buffer[2] >> buffer[3]) {
        for (int i = 0; i < 4; ++i) {
            buffer[i] = base64_decode_table[buffer[i]];
        }
        decoded += (buffer[0] << 2) | (buffer[1] >> 4);
        decoded += (buffer[1] << 4) | (buffer[2] >> 2);
        decoded += (buffer[2] << 6) | buffer[3];
    }

    // Eliminar los caracteres de relleno (padding)
    while (decoded.size() && decoded.back() == '=') {
        decoded.pop_back();
    }

    return decoded;
}