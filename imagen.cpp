
#include <opencv2/highgui.hpp>
#include <opencv2/opencv.hpp>
#include "imagen.h"
#include "base64.h"

Imagen::Imagen(const std::string imagenBase64) {
     std::string imagen_data=base64_decode(imagenBase64);

    // Convertir los datos binarios a una matriz OpenCV (cv::Mat)
    std::vector<unsigned char> buffer{imagen_data.begin(), imagen_data.end()};
    img = cv::imdecode(buffer, cv::IMREAD_COLOR);

    if (img.empty()) {
        std::cout<<"tuve problemas al cargar la imagen"<<std::endl;
    }
}
void Imagen::mostrarImagen() const {
    cv::imshow("imagen",img);
    cv::waitKey(0);
}
