#include <stdexcept>
#include "registro.h"
#include "queryFactory.h"
#include "utils.h"

void Registro::marcar(const RegistroDTO &datos) const{
    //SQL
    //dia y hora en formato por diseno ... porque es mas facil su lectura directa
    // definir SQL 
    std::string sql="INSERT INTO registro_colaboradores(dia,hora,tipo,email,nombre,latitud,longitud,imagen) VALUES(?,?,?,?,?,?,?,?)"; //continuara ..

    //validaciones de datos
    if (datos.email.size()>50) throw std::runtime_error("email invalido");
    if (datos.nombre.size()>250) throw std::runtime_error("nombre invalido");
    if (!validarMarca(datos.email, datos.tipo, fecha {datos.dia,datos.hora})){
        throw std::runtime_error("una marca del mismo tipo para el usuario " + datos.email + " ya fue generada");
    }

    q->ejecutarQuery(sql, [&](){
        // reemplazar datos en el statement
        q->setTextPars(1,datos.dia);
        q->setTextPars(2,datos.hora);
        q->setTextPars(3,datos.tipo);
        q->setTextPars(4,datos.email);
        q->setTextPars(5,datos.nombre);
        q->setRealPars(6,datos.latitud);
        q->setRealPars(7,datos.longitud);
        q->setTextPars(8,datos.imagen);
    }, nullptr);

}

bool Registro::validarMarca(std::string email, std::string tipo, fecha fechaActual) const{
    std::string sql = "select dia, hora from registro_colaboradores where email=? and tipo=? order by dia desc, hora desc limit 1";

    fecha f;

    std::string *s=new std::string("hola");
    
    q->ejecutarQuery(sql, [&](){
        q->setTextPars(1, email);
        q->setTextPars(2, tipo);
    }, [&](){
        f.dia=std::move(q->getText(0));
        f.hora=std::move(q->getText(1));
    });

    return diferenciaHoras(fechaActual,f,3); //3 horas permitidas 
}

Registro::Registro():q{QueryFactory::getQuery()}  {} // relacion de composicion


std::vector<RegistroDTO> Registro::getRegistros(const std::string & dia) const{
    std::vector<RegistroDTO> registros;
    std::string sql="select tipo, email, dia, hora from registro_colaboradores where dia=?";
    q->ejecutarQuery(sql, [&](){
        q->setTextPars(1, dia);
    }, [&](){
        RegistroDTO dto;
        dto.tipo=std::move(q->getText(0));
        dto.email=std::move(q->getText(1));
        dto.dia=std::move(q->getText(2));
        dto.hora=std::move(q->getText(3));
        registros.push_back(dto);
    });
    return registros;
}
