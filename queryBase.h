#pragma once
#include <string>
#include <functional>

//DECLARAR QUE FUNCIONES DEBE TENER CUALQUIER TIPO DE QUERY
class QueryBase {
    public:
        QueryBase()=default;
        virtual ~QueryBase()=default;
        virtual void ejecutarQuery(     std::string sql, 
                                std::function<void()> filtros,
                                std::function<void()> resultados)=0;
        virtual void setTextPars(int col,const std::string & parametro) =0;
        virtual void setRealPars(int col, double valor) =0;

        virtual std::string getText(int col) const=0;
        virtual double getReal(int col) const=0;

};
