#include <regex>
#include "utils.h"

//si puedo o no puedo ingresar la marca de ingreso o egreso de la persona
bool diferenciaHoras(fecha f1, fecha f2, int DIFERENCIANOPERMTIDA) {
    std::regex rdia { "(\\d{4})-(0?[1-9]|1[0-2])-(0?[1-9]|[1-2][0-9]|3[0-1])" };    

    int anio2,anio1, mes2, mes1,dia2,dia1;
    if (std::smatch m; std::regex_match(f1.dia,m,rdia)){
        anio1=std::stoi(m[1]);
        mes1=std::stoi(m[2]);
        dia1=std::stoi(m[3]);
    } else {
        return false;
    }

    if (std::smatch m; std::regex_match(f2.dia,m,rdia)){
        anio2=std::stoi(m[1]);
        mes2=std::stoi(m[2]);
        dia2=std::stoi(m[3]);
    } else {
        return false;
    }

    if (anio2!= anio1 || mes2!=mes1 || std::abs(dia2-dia1)>1) return true;

    int hh2, hh1;
    hh1=std::stoi(f1.hora);
    hh2=std::stoi(f2.hora);

    if (dia2==dia1){
        return std::abs(hh2-hh1)>DIFERENCIANOPERMTIDA;
    } else {
        if (hh2>hh1) {
            return hh1+24-hh2>DIFERENCIANOPERMTIDA;
        } else {
            return hh2+24-hh1>DIFERENCIANOPERMTIDA;
        }
    }

}
