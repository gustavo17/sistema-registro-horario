#pragma once

#include <sqlite3.h>
#include <memory>

//SINGLETON
class DB {
    public:
       static std::unique_ptr<DB> & getInstance(); // crea un DB -> constructor la primera vez y luego reutiliza la misma instancia

       sqlite3 *getConnection(); // usar() ... obtiene la conexion a la base de datos para usos futuros

       virtual ~DB(); //desconecta de la base de datos

     private:
        static inline std::unique_ptr<DB> instancia; //guarda el puntero a la instancia singleton
       

        DB() ;      //conecta a la base de datos
        sqlite3 * db; //almacenar la conexion a la base de datos
       
};