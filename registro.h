#pragma once

#include <string>
#include <vector>
#include "utils.h"
#include "queryBase.h"

struct RegistroDTO { //DTO Data Transfer Object ... Value Object ... La bolsa del supermercado
    std::string dia;
    std::string hora;
    std::string tipo;
    std::string email;
    double latitud;
    double longitud;
    std::string imagen;
    std::string nombre;
    //al agregar parametros
};

class Registro {
    public:
      Registro();
      bool validarMarca(std::string email, std::string tipo,fecha fechaActual) const;
      void marcar(const RegistroDTO &datos) const;
      std::vector<RegistroDTO> getRegistros(const std::string & dia) const;

    private:
      const std::unique_ptr<QueryBase> q;
};