#pragma once

#include <string>


const std::string base64_chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

std::string base64_decode(const std::string& encoded_string);
std::string base64_encode(const unsigned char *bytes_to_encode, unsigned int in_len);
